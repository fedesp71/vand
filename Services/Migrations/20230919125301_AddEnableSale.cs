﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Services.Migrations
{
    public partial class AddEnableSale : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isInSale",
                table: "Products",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isInSale",
                table: "Products");
        }
    }
}
