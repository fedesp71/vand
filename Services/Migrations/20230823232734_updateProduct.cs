﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Services.Migrations
{
    public partial class updateProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImageFile_Products_ProductId",
                table: "ImageFile");

            migrationBuilder.DropIndex(
                name: "IX_ImageFile_ProductId",
                table: "ImageFile");

            migrationBuilder.AddColumn<int>(
                name: "Picture64Id",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Picture64ImageFileId",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_Picture64ImageFileId",
                table: "Products",
                column: "Picture64ImageFileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ImageFile_Picture64ImageFileId",
                table: "Products",
                column: "Picture64ImageFileId",
                principalTable: "ImageFile",
                principalColumn: "ImageFileId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ImageFile_Picture64ImageFileId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_Picture64ImageFileId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Picture64Id",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Picture64ImageFileId",
                table: "Products");

            migrationBuilder.CreateIndex(
                name: "IX_ImageFile_ProductId",
                table: "ImageFile",
                column: "ProductId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ImageFile_Products_ProductId",
                table: "ImageFile",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
