﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Services.Migrations
{
    public partial class modificarProducto2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pictures",
                table: "Products");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Pictures",
                table: "Products",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
