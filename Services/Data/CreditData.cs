﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Data
{
    public class CreditData
    {
        public int Id { get; set; }

        public int Number { get; set; }

        public User UserOwner { get; set; }
    }
}
