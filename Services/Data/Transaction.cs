﻿using Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Data
{
    public class Transaction
    {
        private readonly UserService _userService;
        public Transaction(Product product, User user, UserService userService)
        {
            _userService = userService;
            this.Product = product;
            this.Seller = user;
            this.Buyer = _userService.GetUser();
            this.Comision = 5;
            this.CreditData = new CreditData { Number = 2, UserOwner = this.Buyer };
            this.VandEarning = (this.Comision / 100) * decimal.Parse(this.Product.Price);
        }

        public Transaction()
        {
        }
        public int Id { get; set; }
        public Product Product { get; set; }

        public decimal Comision { get; set; }

        public User Seller { get; set; }

        public User Buyer { get; set; }

        public CreditData CreditData { get; set; }

        public decimal VandEarning { get; set; }


    }
}
