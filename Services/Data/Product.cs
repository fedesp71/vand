﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Data
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

        public string State { get; set; }

        public Album Album { get; set; }
        public DateTime Year { get; set; }
        //public ICollection<ImageFile> Pictures { get; set; } = new List<ImageFile>();
        //public int? Picture64Id { get; set; }
        //public ImageFile Picture64 { get; set; }
        public string Picture { get; set; }

        public string Price { get; set; }

        public bool isInSale { get; set; }
        // Propiedad de navegación hacia el usuario propietario
        public User Owner { get; set; }
    }
}
