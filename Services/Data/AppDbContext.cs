﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Data
{
    public class AppDbContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public AppDbContext(DbContextOptions<AppDbContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Album> Albums { get; set; }

        public DbSet<AdminUser> AdminUsers { get; set; }

        public DbSet<Transaction> Transactions { get; set; }


        public DbSet<UserEarnings> UserEarnings { get; set; }
        //public DbSet<ImageFile> ImageFiles { get; set; }
    }
}
