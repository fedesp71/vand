﻿using Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class TransactionService
    {
        private readonly AppDbContext _dbContext;

        public TransactionService(AppDbContext dbContext, UserService userService)
        {
            _dbContext = dbContext;
            _userService = userService;
        }
        private readonly UserService _userService;

        public async Task GenerateTransaction(Product product)
        {
            var user = product.Owner;
            await _userService.BuyProduct(product);
            await _userService.SellProduct(product, user);
            Transaction transaction = new Transaction(product, user, _userService);
            try
            {
                _dbContext.Transactions.Add(transaction);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            user.UserEarnings.Add(
                new UserEarnings
                {
                    UserId = user.Id,
                    Amount = decimal.Parse(product.Price) - transaction.VandEarning,
                    Date = DateTime.UtcNow,
                });

            try
            {
                _dbContext.UserEarnings.Add(user.UserEarnings.Last());
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
