﻿using Microsoft.EntityFrameworkCore;
using Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class ProductService
    {
        private readonly AppDbContext _dbContext;
        private readonly UserService _userService;
        public ProductService(AppDbContext dbContext, UserService userService)
        {
            _dbContext = dbContext;
            _userService = userService;
        }
        //private UserService userService { get; set; }

        public class UserProductsResult
        {
            public List<Product> Products { get; set; }
            public string Message { get; set; }
        }

        public async Task<bool> UploadProductAsync(Product product)
        {
            try
            {
                _userService.GetUser().Productos.Add(product);

                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex) 
            {
                return false;
            }



            
        }


        public  List<Product> GetAllProductsAsync()
        {
            try
            {
                //var products =  _dbContext.Products.ToList();
                var products = _dbContext.Products.Include(p => p.Album).Include(p => p.Owner).ThenInclude(o => o.UserEarnings).ToList();
                return products;
            }
            catch (Exception ex)
            {
                // Manejo de errores aquí (registro, notificación, etc.)
                return null;
            }
        }

        public  Product GetProductWithAlbumById(int productId)
        {
            try
            {
                var productWithAlbum =  _dbContext.Products
                    .Include(p => p.Album) // Incluir el álbum relacionado
                    .FirstOrDefault(p => p.Id == productId);

                return productWithAlbum;
            }
            catch (Exception ex)
            {
                // Manejo de errores aquí (registro, notificación, etc.)
                return null;
            }
        }


        public UserProductsResult GetProductsByUserIdAsync()
        {
            try
            {
                
                var user = _userService.GetUser();

                if (user != null && user.Productos == null)
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "Aún no tienes productos cargados."
                    };
                }

                if (user != null)
                {
                    return new UserProductsResult
                    {
                        Products = user.Productos.ToList(),
                        Message = "Productos encontrados correctamente."
                    };
                }
                return new UserProductsResult
                {
                    Products = new List<Product>(),
                    Message = "Debes estar logueado para ver esto."
                };
            }
            catch (Exception ex)
            {
                // Manejo de errores aquí (registro, notificación, etc.)
                return new UserProductsResult
                {
                    Products = null,
                    Message = "Se produjo un error al recuperar los productos."
                };
            }
        }

        public UserProductsResult GetProductsNotBelongingToUserAsync33()
        {
            try
            {
                var user = _userService.GetUser();

                if (user == null)
                {
                    return new UserProductsResult
                    {
                        Products = null,
                        Message = "Debes estar logueado para ver esto."
                    };
                }

                var allProducts = this.GetAllProductsAsync(); // Obtén todos los productos disponibles

                if (allProducts == null || !allProducts.Any())
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "No hay productos disponibles en el sistema."
                    };
                }
                //var productsNotBelongingToUser = allProducts.Where(product => product.OwnerId != user.Id).ToList();
                var productsNotBelongingToUser = allProducts
                    .Where(product => !user.Productos.Any(up => up.Id == product.Id))
                    .ToList();
                //var productsNotBelongingToUser = allProducts.Where(product => product.OwnerId != user.Id).ToList();

                if (!productsNotBelongingToUser.Any())
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "No hay productos disponibles que no sean de tu usuario."
                    };
                }

                return new UserProductsResult
                {
                    Products = productsNotBelongingToUser,
                    Message = "Productos encontrados correctamente."
                };
            }
            catch (Exception ex)
            {
                // Manejo de errores aquí (registro, notificación, etc.)
                return new UserProductsResult
                {
                    Products = null,
                    Message = "Se produjo un error al recuperar los productos."
                };
            }
        }

        public Product GetProductById(int productId)
        {
            try
            {
                var product = _dbContext.Products
                    .Include(p => p.Album) // Incluir el álbum relacionado si es necesario
                    .FirstOrDefault(p => p.Id == productId);

                return product;
            }
            catch (Exception ex)
            {
                // Manejo de errores aquí (registro, notificación, etc.)
                return null;
            }
        }




        public UserProductsResult GetProductsNotBelongingToUserAsync2()
        {
            try
            {
                var user = _userService.GetUser();

                if (user == null)
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "Debes estar logueado para ver esto."
                    };
                }

                var allProducts = this.GetAllProductsAsync(); // Obtén todos los productos disponibles

                if (allProducts == null || !allProducts.Any())
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "No hay productos disponibles en el sistema."
                    };
                }

                //var productsNotBelongingToUser = allProducts.Where(product => product.Id != user.Id).ToList();
                var productsNotBelongingToUser = allProducts
                    .Where(product => !user.Productos.Any(userProduct => userProduct.Id == product.Id))

                    .ToList();

                if (!productsNotBelongingToUser.Any())
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "No hay productos disponibles que no sean de tu usuario."
                    };
                }

                return new UserProductsResult
                {
                    Products = productsNotBelongingToUser,
                    Message = "Productos encontrados correctamente."
                };
            }
            catch (Exception ex)
            {
                // Manejo de errores aquí (registro, notificación, etc.)
                return new UserProductsResult
                {
                    Products = null,
                    Message = "Se produjo un error al recuperar los productos."
                };
            }
        }

        public UserProductsResult GetProductsNotBelongingToUserAsync()
        {
            try
            {
                var user = _userService.GetUser();

                if (user == null)
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "Debes estar logueado para ver esto."
                    };
                }

                var allProducts = this.GetAllProductsAsync(); // Obtén todos los productos disponibles

                if (allProducts == null || !allProducts.Any())
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "No hay productos disponibles en el sistema."
                    };
                }

                var productsNotBelongingToUser = allProducts
                    .Where(product => product.Owner == null || product.Owner.Id != user.Id)
                    .ToList();

                if (!productsNotBelongingToUser.Any())
                {
                    return new UserProductsResult
                    {
                        Products = new List<Product>(),
                        Message = "No hay productos disponibles que no sean de tu usuario."
                    };
                }

                return new UserProductsResult
                {
                    Products = productsNotBelongingToUser,
                    Message = "Productos encontrados correctamente."
                };
            }
            catch (Exception ex)
            {
                // Manejo de errores aquí (registro, notificación, etc.)
                return new UserProductsResult
                {
                    Products = null,
                    Message = "Se produjo un error al recuperar los productos."
                };
            }
        }


        public bool ChangeProductSatus (Product product)
        {
            try
            {
                var user = _userService.GetUser();

                user.Productos.FirstOrDefault(p => p.Id == product.Id).isInSale = !product.isInSale;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }



    }
}
