﻿using Microsoft.EntityFrameworkCore;
using Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class UserService
    {
        private readonly AppDbContext _dbContext;

        private User user;


        public UserService(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task RegisterUserAsync(string name, string email, string password)
        {
            var newUser = new User
            {
                Name = name,
                Email = email,
                Password = password,
                Productos = new List<Product>(),
                UserEarnings = new List<UserEarnings>()
            };
            try
            {
                _dbContext.Users.Add(newUser);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public User GetUserByEmailAndPassword(string email, string password)
        {
            if (user != null)
            {
                return user;
            }
            else
            {
                user = _dbContext.Users
                    .Include(u  => u.UserEarnings)
                    .Include(u => u.Productos) // Esto carga los productos del usuario.
                        .ThenInclude(p => p.Album) // Esto carga los datos del álbum de cada producto.
                      
                    .FirstOrDefault(u => u.Email == email && u.Password == password);
                return user;
            }
        }

        public async Task BuyProduct(Product product)
        {
            product.isInSale = false;
            product.Owner = user;
            this.user.Productos.Add(product);

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task SellProduct(Product product, User user)
        {
            this.user.Productos.Remove(product);

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public string GetUserName()
        {
            return user == null ? "" : user.Name;

        }

        public string GetUserNameLogin()
        {
            return user == null ? "Inicie sesión" : user.Name;

        }

        public int GetUserID()
        {
            return user == null ? 0 : user.Id;

        }

        public User GetUser()
        { return user; }

        public bool IsUserLoggedIn()
        {
            return user != null;
        }

        public void Logout ()
        {
            user = null;
        }
    }
}
