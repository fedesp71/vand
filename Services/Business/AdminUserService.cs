﻿using Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class AdminUserService
    {
        private readonly AppDbContext _dbContext;

        private AdminUser adminUser;

        public AdminUserService(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task RegisterUserAsync(string name, string email, string password)
        {
            var newUser = new AdminUser
            {
                Name = name,
                Email = email,
                Password = password
            };
            try
            {
                _dbContext.AdminUsers.Add(newUser);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public AdminUser GetUserByEmailAndPassword(string email, string password)
        {
            if (adminUser != null)
            {
                return adminUser;
            }
            else
            {
                adminUser = _dbContext.AdminUsers.FirstOrDefault(u => u.Email == email && u.Password == password);
                return adminUser;
            }
        }


        public string GetUserName()
        {
            return adminUser == null ? "" : adminUser.Name;

        }
    }
}
