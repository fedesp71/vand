// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace VandUser.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using VandUser;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using VandUser.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\ListProducts.razor"
using Services.Business;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\ListProducts.razor"
using Services.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\ListProducts.razor"
using static Services.Business.ProductService;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Components.RouteAttribute("/lista-productos")]
    public partial class ListProducts : global::Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 144 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\ListProducts.razor"
       
    private string filtroNombre = "";
    private string filtroArtista = "";
    private string filtroAño = "";
    private List<Product> productosFiltrados;

    [Inject]
    private ProductService _productService { get; set; }

    // Aquí debes tener una lista de productos (puedes reemplazar esto con tus datos reales)
    //private List<Product> productos = new List<Product> 
    //{
    //    new Product
    //    {
    //        Id = 1,
    //        Album = new Album { Name = "Album1", Artist = "Artista1" },
    //        Year = DateTime.Now,
    //        Description = "Descripción del producto 1"
    //    },
    //    new Product
    //    {
    //        Album = new Album { Name = "Album2", Artist = "Artista2" },
    //        Year = DateTime.Now,
    //        Description = "Descripción del producto 2"
    //    },
    //    // ...
    //};
    private List<Product> productos = new List<Product>();
    protected override void OnInitialized()
    {
        ObtenerProductos();
        FiltrarProductos();
    }

    private void FiltrarProductos()
    {
        productosFiltrados = productos.Where(producto =>
            producto.Album.Name.Contains(filtroNombre, StringComparison.OrdinalIgnoreCase) &&
            producto.Album.Artist.Contains(filtroArtista, StringComparison.OrdinalIgnoreCase) &&
            (string.IsNullOrEmpty(filtroAño) || producto.Year.Year.ToString() == filtroAño))
            .ToList();
    }

    private void VerDetalle(Product producto)
    {
        // Aquí puedes implementar la lógica para mostrar el detalle del producto
        // Puede ser una redirección a otra página o un cuadro de diálogo, por ejemplo
        NavigationManager.NavigateTo($"/detalle-producto/{producto.Id.ToString()}");
    }

    private void Buscar()
    {
        FiltrarProductos();
    }

    private void ObtenerProductos()
    {
        UserProductsResult userProductsResult = new UserProductsResult();
        userProductsResult = _productService.GetProductsNotBelongingToUserAsync();
        if (userProductsResult.Products.Count > 0)
        {
            productos = userProductsResult.Products;
        }
        else
        {
            message = userProductsResult.Message;
        }
    }

    private bool isErrorMessage = false;

    private void HandleMessageChange()
    {
        isErrorMessage = !string.IsNullOrWhiteSpace(message) && message.ToLower().Contains("error");
    }

    private string message = "No se encontraron productos.";

    private string Message
    {
        get => message;
        set
        {
            message = value;
            HandleMessageChange();
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
    }
}
#pragma warning restore 1591
