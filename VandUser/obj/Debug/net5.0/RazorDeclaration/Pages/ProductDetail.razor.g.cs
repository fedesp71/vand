// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace VandUser.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using VandUser;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using VandUser.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\ProductDetail.razor"
using Services.Business;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\ProductDetail.razor"
using Services.Data;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Components.RouteAttribute("/detalle-producto/{productoId}")]
    public partial class ProductDetail : global::Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 142 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\ProductDetail.razor"
       
    [Parameter]
    public string ProductoId { get; set; }

    private Product producto;


    private List<string> imagenes;
    private int imagenIndex;
    private string imagenActual;

    [Inject]
    private UserService userService { get; set; }

    [Inject]
    private ProductService _productService { get; set; }

    private void CambiarImagenAnterior()
    {
        imagenIndex = (imagenIndex - 1 + imagenes.Count) % imagenes.Count;
        imagenActual = imagenes[imagenIndex];
    }

    private void CambiarImagenSiguiente()
    {
        imagenIndex = (imagenIndex + 1) % imagenes.Count;
        imagenActual = imagenes[imagenIndex];
    }

    private List<string> ObtenerImagenesDelProducto(int productoId)
    {
        // Aquí deberías implementar la lógica para obtener las imágenes del producto
        // Retorna una lista de URLs de imágenes relacionadas con el producto
        // Ejemplo:
        return new List<string>
    {
            "https://contentv2.tap-commerce.com/cover/large/858978005219_1.jpg?id_com=1156",
            "https://http2.mlstatic.com/D_NQ_NP_817731-MLU69139265790_042023-O.webp",
            // ...
        };
    }

    protected override void OnInitialized()
    {
        // Simula la obtención de datos del producto según el ID proporcionado
        // Debes reemplazar esto con tu lógica real para obtener los detalles del producto
        producto = ObtenerProductoPorId(int.Parse(ProductoId));

        // Aquí obtén la lista de imágenes relacionadas con el producto
        imagenes = ObtenerImagenesDelProducto(producto.Id);

        // Establece la imagen actual en la primera de la lista
        imagenIndex = 0;
        imagenActual = imagenes[imagenIndex];
    }

    private Product ObtenerProductoPorId(int id)
    {
        // Aquí deberías implementar la lógica para obtener el producto por su ID
        // Retorna un objeto Product simulado para este ejemplo
        return _productService.GetProductWithAlbumById(id);
        //return new Product
        //{
        //    Album = new Album
        //    {
        //        Name = "Álbum Ejemplo",
        //        Artist = "Artista Ejemplo",

        //    },
        //    Year = DateTime.Now,
        //    //Pictures = "https://contentv2.tap-commerce.com/cover/large/858978005219_1.jpg?id_com=1156",
        //    Description = "Descripción del producto de ejemplo",
        //    //Pictures = new List<ImageFile>().Add()

        //};
    }

    private void IniciarCompra()
    {
        // Implementa la lógica para iniciar la compra del producto
        // Puede ser una redirección a una página de compra o mostrar un formulario de compra
        Navigation.NavigateTo($"/inicio-compra/{producto.Id}");
    }

    private void CambiarEstado()
    {

        _productService.ChangeProductSatus(producto);
        Navigation.NavigateTo($"/detalle-producto/{producto.Id}");
    }

    private void Volver()
    {
        if (!userService.GetUser().Productos.Contains(producto))
        {
            Navigation.NavigateTo("/lista-productos");
        }
        else
        {
            Navigation.NavigateTo("/mis-productos");
        }
        
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager Navigation { get; set; }
    }
}
#pragma warning restore 1591
