// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace VandUser.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using VandUser;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\_Imports.razor"
using VandUser.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\Register.razor"
using Services.Business;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Components.RouteAttribute("/registro")]
    public partial class Register : global::Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 36 "C:\Fede\Personal\UAI\VandNet5\VAND\VandUser\Pages\Register.razor"
       
    private string nombre;
    private string email;
    private string password;
    private bool hasSubmitted = false;

    [Inject]
    private UserService _userService { get; set; }
    private async Task Registrarse()
    {
        hasSubmitted = true;

        // Verificar si hay errores antes de continuar
        if (string.IsNullOrWhiteSpace(nombre) || string.IsNullOrWhiteSpace(email) || !IsValidEmail(email) || string.IsNullOrWhiteSpace(password))
        {
            return; // No hagas nada si hay errores
        }
        await _userService.RegisterUserAsync(nombre, email, password);
        // Aquí puedes agregar la lógica para registrar al usuario
        // por ejemplo, enviar los datos al servidor
        // y redirigir a otra página después del registro exitoso

        // Por ahora, solo mostraremos un mensaje de éxito en la consola
        Console.WriteLine("Registro exitoso!");
        NavigationManager.NavigateTo("/registro-exitoso");
    }

    private bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
    }
}
#pragma warning restore 1591
