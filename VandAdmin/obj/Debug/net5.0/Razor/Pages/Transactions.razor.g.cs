#pragma checksum "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9f1526abf4b4752320604c4ca73810913f48603f"
// <auto-generated/>
#pragma warning disable 1591
namespace VandAdmin.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using VandAdmin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using VandAdmin.Shared;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Components.RouteAttribute("/transacciones")]
    public partial class Transactions : global::Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h1>Transacciones</h1>\r\n\r\n");
            __builder.OpenElement(1, "table");
            __builder.AddAttribute(2, "class", "table");
            __builder.AddMarkupContent(3, "<thead><tr><th>Comprador</th>\r\n            <th>Vendedor</th>\r\n            <th>Fecha</th>\r\n            <th>Monto</th>\r\n            <th>Comisión</th></tr></thead>\r\n    ");
            __builder.OpenElement(4, "tbody");
#nullable restore
#line 16 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor"
         foreach (var transaccion in transacciones)
        {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(5, "tr");
            __builder.OpenElement(6, "td");
#nullable restore
#line 19 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor"
__builder.AddContent(7, transaccion.Comprador);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(8, "\r\n                ");
            __builder.OpenElement(9, "td");
#nullable restore
#line 20 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor"
__builder.AddContent(10, transaccion.Vendedor);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\r\n                ");
            __builder.OpenElement(12, "td");
#nullable restore
#line 21 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor"
__builder.AddContent(13, transaccion.Fecha.ToString("yyyy-MM-dd HH:mm:ss"));

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(14, "\r\n                ");
            __builder.OpenElement(15, "td");
#nullable restore
#line 22 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor"
__builder.AddContent(16, transaccion.Monto.ToString("C"));

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\r\n                ");
            __builder.OpenElement(18, "td");
#nullable restore
#line 23 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor"
__builder.AddContent(19, transaccion.Comision.ToString("C"));

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 25 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor"
        }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(20, "\r\n\r\n");
            __builder.AddMarkupContent(21, @"<style>
    .table {
        width: 100%;
        border-collapse: collapse;
        margin-top: 20px;
    }

        .table th, .table td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        .table th {
            background-color: #f0f5f9;
        }

        .table tr:nth-child(even) {
            background-color: #f7f7f7;
        }
</style>");
        }
        #pragma warning restore 1998
#nullable restore
#line 51 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Transactions.razor"
       
    private List<Transaccion> transacciones = new List<Transaccion>
    {
        new Transaccion { Comprador = "Usuario1", Vendedor = "Usuario2", Fecha = DateTime.Now.AddDays(-1), Monto = 50.00m, Comision = 5.00m },
        new Transaccion { Comprador = "Usuario2", Vendedor = "Usuario3", Fecha = DateTime.Now, Monto = 75.50m, Comision = 7.50m },
        new Transaccion { Comprador = "Usuario3", Vendedor = "Usuario1", Fecha = DateTime.Now.AddHours(-2), Monto = 120.75m, Comision = 12.00m }
    };

    public class Transaccion
    {
        public string Comprador { get; set; }
        public string Vendedor { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Monto { get; set; }
        public decimal Comision { get; set; }
    }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
