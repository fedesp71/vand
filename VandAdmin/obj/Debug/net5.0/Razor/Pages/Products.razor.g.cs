#pragma checksum "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5c77cf7de1c0cf0b4707a31c3b12d7edc857c7e1"
// <auto-generated/>
#pragma warning disable 1591
namespace VandAdmin.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using VandAdmin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\_Imports.razor"
using VandAdmin.Shared;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Components.RouteAttribute("/productos")]
    public partial class Products : global::Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h1>Administrar Productos</h1>\r\n\r\n");
            __builder.OpenElement(1, "table");
            __builder.AddAttribute(2, "class", "table");
            __builder.AddMarkupContent(3, "<thead><tr><th>ID</th>\r\n            <th>Nombre</th>\r\n            <th>Artista</th>\r\n            <th>Usuario</th>\r\n            <th></th></tr></thead>\r\n    ");
            __builder.OpenElement(4, "tbody");
#nullable restore
#line 16 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
         foreach (var producto in productos)
        {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(5, "tr");
            __builder.OpenElement(6, "td");
#nullable restore
#line 19 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
__builder.AddContent(7, producto.Id);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(8, "\r\n                ");
            __builder.OpenElement(9, "td");
#nullable restore
#line 20 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
__builder.AddContent(10, producto.Name);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\r\n                ");
            __builder.OpenElement(12, "td");
#nullable restore
#line 21 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
__builder.AddContent(13, producto.Artist);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(14, "\r\n                ");
            __builder.OpenElement(15, "td");
#nullable restore
#line 22 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
__builder.AddContent(16, producto.Usuario);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\r\n                ");
            __builder.OpenElement(18, "td");
            __builder.OpenElement(19, "button");
            __builder.AddAttribute(20, "onclick", global::Microsoft.AspNetCore.Components.EventCallback.Factory.Create<global::Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 24 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
                                      () => EditarProducto(producto)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(21, "class", "btn btn-edit");
            __builder.AddContent(22, "Editar");
            __builder.CloseElement();
            __builder.AddMarkupContent(23, "\r\n                    ");
            __builder.OpenElement(24, "button");
            __builder.AddAttribute(25, "onclick", global::Microsoft.AspNetCore.Components.EventCallback.Factory.Create<global::Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 25 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
                                      () => EliminarProducto(producto)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(26, "class", "btn btn-delete");
            __builder.AddContent(27, "Eliminar");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 28 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
        }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(28, "\r\n\r\n");
            __builder.AddMarkupContent(29, @"<style>
    /* Estilos de la tabla */
    .table {
        width: 100%;
        border-collapse: collapse;
        margin-top: 20px;
    }

        .table th, .table td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        .table th {
            background-color: #f0f5f9;
        }

        .table tr:nth-child(even) {
            background-color: #f7f7f7;
        }

    /* Estilos de los botones */
    .btn {
        padding: 6px 12px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        font-size: 14px;
        transition: background-color 0.3s;
    }

    .btn-edit {
        background-color: #3490dc;
        color: #fff;
        margin-right: 6px;
    }

    .btn-delete {
        background-color: #e74c3c;
        color: #fff;
    }

    .btn:hover {
        opacity: 0.85;
    }
</style>");
        }
        #pragma warning restore 1998
#nullable restore
#line 80 "C:\Fede\Personal\UAI\VandNet5\VAND\VandAdmin\Pages\Products.razor"
       
    private List<Producto> productos = new List<Producto>
    {
        new Producto { Id = 1, Name = "Album1", Artist = "Artista1", Usuario = "Usuario1" },
        new Producto { Id = 2, Name = "Album2", Artist = "Artista2", Usuario = "Usuario2" },
        new Producto { Id = 3, Name = "Album3", Artist = "Artista3", Usuario = "Usuario3" }
    };

    public class Producto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Usuario { get; set; }
    }

    private void AgregarProducto()
    {
        // Lógica para agregar un nuevo producto
    }

    private void EditarProducto(Producto producto)
    {
        // Lógica para editar el producto seleccionado
    }

    private void EliminarProducto(Producto producto)
    {
        // Lógica para eliminar el producto seleccionado
    }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
